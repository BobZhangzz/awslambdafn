# Create a simple AWS Lambda function that processes data
In this mini project, we are going to create a simple rust function that perfrom some data processing and deploy to AWS using `cargo lambda`. 

# Procedures:
The following demonstrate the general steps that one may following to leverage the functionality of `cargo lambda`.
## Step 1:
You may choose to work on any IDE you like. I used AWS Cloud 9. First of all, ensure you have rust installed in the cloud9; if not, use the following command line in the terminal:
```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```
## Step 2
Install `cargo lambda` using `brew`
```
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```
## Step 3:
The new subcommand will help you create a new project with a default template. When that's done, change into the new directory. You can name the project yourself. Here I named it `new-lambda-project`.
```
cargo lambda new new-lambda-project \
    && cd new-lambda-project
```
## Step 4:
Use the build subcommand to compile your function for Linux systems:
```
cargo lambda build --release
```
## Step 5:
Use the deploy subcommand to upload your function to AWS Lambda. This subcommand requires AWS credentials in your system.
```
cargo lambda deploy
```
![Alt Text](/img/Screenshot_2024-03-13_at_19.57.39.png)
![Alt Text](img/Screenshot_2024-03-13_at_23.39.12.png)

